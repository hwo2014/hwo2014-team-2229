package protocol

const (
	SwitchLeft = "Left"
	SwitchRight = "Right"
)

type SwitchLane string