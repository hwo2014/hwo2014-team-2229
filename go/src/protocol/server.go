package protocol

// JOIN message data
type Join struct {
	Name string
	Key string
}

type CarId struct {
	Name string
	Color string
}

// YOURCAR message data
type YourCar CarId

// GAMEINIT message data
type GameInit struct {
	Race struct {
		Track Track
		Cars []Car
		RaceSession RaceSession
	}
}

type Track struct {
	Id string
	Name string
	Pieces []Piece
	Lanes []Lane
	StartingPoint struct {
		Position Pos
		Angle float32
	}
}

type Piece struct {
	Length float32
	Switch bool
	Radius float32
	Angle  float32
}

type Lane struct {
	Index int32
	DistanceFromCenter float32
}

type Pos struct {
	X float32
	Y float32
}

type Car {
	Id CarId
	Dimensions struct {
		Length float32
		Width float32
		GuideFlagPosition float32
	}
}

type RaceSession struct {
	Laps int32
	MaxLapTimeMs float32
	QuickRace bool
}

// GAMESTART message data
type GameStart struct {}

// CARPOSITIONS message data
type CarPositions []CarPosition
type CarPosition struct {
	Id CarId
	Angle float32
	PiecePosition PiecePosition
}

type PiecePosition struct {
	PieceIndex int32
	InPieceDistance float32
	Lane struct {
		StartLaneIndex int32
		EndLaneIndex int32
	}
	Lap int32
}

// GAMEEND message data
type GameEnd struct {
	Results []Result
	BestLaps []Result
}

type Result struct {
	Car CarId
	Result *RaceTime
}

type RaceTime struct {
	Laps int32
	Ticks int32
	Millis int32
}

// TOURNAMENTEND message data
type TournamentEnd struct {}

// CRASH message data
type Crash CarId

// SPAWN message data
type Spawn CarId

// LAPFINISHED message data
type LapFinished struct {
	Car CarId
	LapTime LapTime
	RaceTime RaceTime
	Ranking Ranking
}

type LapTime struct {
	Lap int32
	Ticks int32
	Millis int32
}

type Ranking struct {
	Overall int32
	FastestLap int32
}

// DNF message data
type DNF struct {
	Car CarId
	Reason string
}

// FINISH message data
type Finish struct CarId