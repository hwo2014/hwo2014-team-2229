package main

import (
	"encoding/json"
	"log"
	"math"
	"os"
)

type Markers struct {
	M  []Marker
	At []int
}

type Marker struct {
	// Piece information
	Index   int   `json:"index"`
	StartAt float `json:"startAt"`
	Length  float `json:"length"`
	Angle   float `json:"angle"`
	Radius  float `json:"radius"`

	MaxEntrySpeed float `json:"maxEntrySpeed"`
	TargetSpeed   float `json:"targetSpeed"`

	DriftLeft  float `json:"driftLeft"`
	DriftRight float `json:"driftRight"`
}

// Interesting handlers
func (s *State) HandleGameInit(m *Inbound) error {
	s.PrevTick = 0
	s.Tick = 0

	init := &GameInit{}
	err := json.Unmarshal(m.Data, init)
	if err != nil {
		log.Printf("Error: gameInit %v", string(m.Data))
		return err
	}

	s.Track = init.Race.Track
	for _, info := range init.Race.Cars {
		s.Cars[info.Id.Name] = info
	}
	s.Session = init.Race.Session

	s.Markers = NewMarkers(s.Track.Pieces)
	s.Markers.Update(s)

	if *verbose {
		f, _ := os.Create("markers_" + *track + ".js")
		f.Write([]byte("var " + *track + " = "))
		data, _ := json.MarshalIndent(s, "", "    ")
		f.Write(data)
		f.Write([]byte(";"))
		f.Close()
	}

	return s.ping()
}

func (s *State) HandleGameStart(m *Inbound) error {
	s.Bot.Send(Throttle(1.0))
	return nil
}

func NewMarkers(pieces []Piece) *Markers {
	firstat := make([]int, len(pieces))
	markers := make([]Marker, 0, len(pieces)*3)
	// average marker size
	unlimited := float(100.0)
	length := float(30.0)
	for i, p := range pieces {
		startat := float(0.0)

		total := p.Length
		if p.Radius > 0 {
			total = 2 * math.Pi * p.Radius * abs(p.Angle) / 360.0
		}
		size := total

		firstat[i] = len(markers)

		for size > 0 {
			// stupid way
			speed := unlimited
			if p.Radius > 0 {
				speed = 0.5
			}

			curlen := min(length, size)
			m := Marker{
				Index:   i,
				StartAt: startat,
				Length:  curlen,
				Angle:   p.Angle * curlen / total,
				Radius:  p.Radius,

				MaxEntrySpeed: speed,
				TargetSpeed:   speed,
			}

			markers = append(markers, m)
			size -= length
			startat += length
		}
	}

	return &Markers{markers, firstat}
}

func (m *Markers) calculateCriticalDrifts(s *State) {
	M := m.M
	N := len(M) - 1

	falloff := float(1 / 2)
	distance := float(70)

	for i := N - 1; i >= 0; i -= 1 {
		next := &M[i]
		cur := &M[(i-1+N)%N]

		angle := radians(cur.Angle)

		m := pow(falloff, cur.Length/distance)
		cur.DriftLeft = (angle + next.DriftLeft) * m
		cur.DriftRight = (angle + next.DriftRight) * m

		cur.DriftLeft = max(cur.DriftLeft, angle*m)
		cur.DriftRight = min(cur.DriftRight, -angle*m)

		cur.DriftLeft *= 0.9
		cur.DriftRight *= 0.9
	}
}

func (m *Markers) calculateMaxSpeeds(s *State) {
	M := m.M
	N := len(M) - 1
	for i := N - 1; i >= 0; i -= 1 {
		next := &M[i]
		cur := &M[(i-1+N)%N]

		df := delta(next.DriftLeft, next.DriftRight)
		cur.TargetSpeed = exp(-df)
	}
}

func (m *Markers) smoothenSpeeds(s *State) {
	M := m.M
	N := len(M) - 1
	for i := N - 1; i >= 0; i -= 1 {
		next := &M[i]
		cur := &M[(i-1+N)%N]

		r := pow(1/2, cur.Length/100)
		cur.TargetSpeed = min(cur.TargetSpeed, cur.TargetSpeed*r+next.TargetSpeed*(1-r))
	}
}

func (m *Markers) Update(s *State) {
	m.calculateCriticalDrifts(s)
	m.calculateCriticalDrifts(s)
	m.calculateMaxSpeeds(s)
	m.smoothenSpeeds(s)
}

func (m *Markers) FindCurrentNext(index int, distance float) (cur *Marker, indist float, next *Marker) {
	M := m.M
	N := len(M)
	i := m.At[index]
	indist = distance
	cur = &M[i]
	for indist > cur.Length {
		indist -= cur.Length
		i = (i + 1) % N
		cur = &M[i]
	}

	next = &M[(i+1)%N]
	return
}

func (s *State) HandleCarPositions(m *Inbound) error {
	positions := CarPositions{}
	err := json.Unmarshal(m.Data, &positions)
	if err != nil {
		log.Printf("Error: carPositions %v", string(m.Data))
		return err
	}

	var pos PiecePosition

	for _, car := range positions {
		if car.Id.Name == s.Car.Name {
			s.LastPosition = car
			pos = car.Piece
			break
		}
	}

	cur, dist, next := s.Markers.FindCurrentNext(pos.Index, pos.Distance)

	prop := dist / cur.Length

	_, _ = next, prop
	// interpolate the best speed
	throttle := cur.TargetSpeed //*(1-prop) + min(next.TargetSpeed, 1.0)*prop
	// time := (next.TargetSpeed - maxspeed) / Deceleration
	throttle = min(throttle, 1.0)

	s.LastThrottle = throttle
	return s.Bot.Send(Throttle(throttle))
}
