package main

import "encoding/json"

type Inbound struct {
	Type     string          `json:"msgType"`
	Data     json.RawMessage `json:"data"`
	GameId   string          `json:"gameId"`
	GameTick int             `json:"gameTick"`
}

type Message interface {
	Type() string
}

type BotId struct {
	Name string `json:"name"`
	Key  string `json:"key"`
}

type Join struct {
	BotId
}

func (m *Join) Type() string { return "join" }

type CreateRace struct {
	Id       BotId  `json:"botId"`
	Track    string `json:"trackName"`
	Password string `json:"password"`
	Cars     int    `json:"carCount"`
}

func (m *CreateRace) Type() string { return "createRace" }

type JoinRace struct {
	Id       BotId  `json:"botId"`
	Track    string `json:"trackName"`
	Password string `json:"password"`
	Cars     int    `json:"carCount"`
}

func (m *JoinRace) Type() string { return "joinRace" }

type Ping struct{}

func (p *Ping) Type() string { return "ping" }

// Client -> Server
// Specifies throttle in range [0.0 .. 1.0]
type Throttle float32

func (t Throttle) Type() string { return "throttle" }

// Server -> Client
type GameInit struct {
	Race Race `json:"race"`
}

func (i *GameInit) Type() string { return "gameInit" }
