package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net"
	"os"
)

type Bot struct {
	Conn net.Conn
	Name string
	Key  string

	In  *bufio.Reader
	Out *bufio.Writer

	EventLog *bufio.Writer

	State *State
	ended bool
}

func NewBot(conn net.Conn, name string, key string) *Bot {
	bot := &Bot{
		Conn:  conn,
		Name:  name,
		Key:   key,
		State: &State{},
	}

	bot.State.Bot = bot

	if *verbose {
		file, _ := os.Create("events_" + *track + ".log")
		bot.EventLog = bufio.NewWriter(file)
	}

	bot.State.Init()
	return bot
}

func (b *Bot) Send(m Message) error {
	msg := make(map[string]interface{})
	msg["msgType"] = m.Type()
	msg["data"] = m

	if b.State.Tick > 0 {
		msg["gameTick"] = b.State.Tick
	}

	data, err := json.Marshal(msg)
	if err != nil {
		log.Printf("Error: marshaling %+v\n", m)
		return err
	}
	data = append(data, '\n')

	_, err = b.Out.Write(data)
	if err != nil {
		log.Printf("Error: sending %+v\n", m)
		return err
	}

	b.Out.Flush()
	return nil
}

func (b *Bot) NextMessage() (*Inbound, error) {
	line, err := b.In.ReadBytes('\n')
	if err != nil {
		log.Printf("Error: reading line\n")
		return nil, err
	}

	if *verbose {
		b.EventLog.Write(line)
		b.EventLog.Flush()
	}

	msg := &Inbound{}
	err = json.Unmarshal(line, msg)
	if err != nil {
		log.Printf("Error: unmarshaling %v\n", string(line))
		return nil, err
	}

	return msg, nil
}

func (b *Bot) RespondPing(m *Inbound) error {
	b.Send(&Ping{})
	return nil
}

func (b *Bot) Shutdown() {
	b.ended = true
}

func (b *Bot) Id() BotId {
	return BotId{b.Name, b.Key}
}

func (b *Bot) Run() {
	b.In = bufio.NewReader(b.Conn)
	b.Out = bufio.NewWriter(b.Conn)

	if *track == "" {
		b.Send(&Join{b.Id()})
	} else {
		b.Send(&CreateRace{b.Id(), *track, *password, 1})
	}

	for !b.ended {
		message, err := b.NextMessage()
		if err == io.EOF {
			break
		}

		log.SetPrefix(fmt.Sprintf("%s:%-5d ", *track, message.GameTick))

		if err != nil {
			log.Fatal(err)
		}

		err = b.State.Handle(message)
		if err != nil {
			log.Fatal(err)
		}
	}
}
