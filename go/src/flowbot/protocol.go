package main

type CarId struct {
	Name  string `json:"name"`
	Color string `json:"color"`
}

type Race struct {
	Track   Track     `json:"track"`
	Cars    []CarInfo `json:"cars"`
	Session Session   `json:"raceSession"`
}

type Piece struct {
	Length float `json:"length"`
	Switch bool  `json:"switch"`
	Radius float `json:"radius"`
	Angle  float `json:"angle"`
}

type Lane struct {
	Index    int   `json:"index"`
	Distance float `json:"distanceFromCenter"`
}

type Pos struct {
	X float `json:"x"`
	Y float `json:"y"`
}

type Track struct {
	Id   string `json:"id"`
	Name string `json:"name"`

	Pieces []Piece `json:"pieces"`
	Lanes  []Lane  `json:"lanes"`

	StartingPoint struct {
		Position Pos   `json:"position"`
		Angle    float `json:"angle"`
	} `json:"startingPoint"`
}

type CarInfo struct {
	Id CarId `json:"id"`

	Dimensions struct {
		Length float `json:"length"`
		Width  float `json:"width"`
		Flag   float `json:"guideFlagPosition"`
	}
}

type Session struct {
	Laps         int  `json:"laps"`
	MaxLapTimeMs int  `json:"maxLapTimeMs"`
	QuickRace    bool `json:"quickRace"`
}

type CarPositions []CarPosition
type CarPosition struct {
	Id    CarId         `json:"id"`
	Angle float         `json:"angle"`
	Piece PiecePosition `json:"piecePosition"`
}

type PiecePosition struct {
	Index    int   `json:"pieceIndex"`
	Distance float `json:"inPieceDistance"`
	Lane     struct {
		Start int `json:"startLaneIndex"`
		End   int `json:"endLaneIndex"`
	} `json:"lane"`
	Lap int `json:"lap"`
}

type LapFinished struct {
	Car      CarId    `json:"car"`
	LapTime  LapTime  `json:"lapTime"`
	RaceTime RaceTime `json:"raceTime"`
	Ranking  Ranking  `json:"ranking"`
}

type RaceTime struct {
	Laps   int `json:"laps"`
	Ticks  int `json:"ticks"`
	Millis int `json:"millis"`
}

type LapTime struct {
	Lap    int `json:"lap"`
	Ticks  int `json:"ticks"`
	Millis int `json:"millis"`
}

type Ranking struct {
	Overall    int `json:"overall"`
	FastestLap int `json:"fastestLap"`
}
