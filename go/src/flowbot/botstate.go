package main

import (
	"encoding/json"
	"log"
)

type Handler func(m *Inbound) error
type HandlerMap map[string]Handler

type Sender interface {
	Id() BotId
	Send(m Message) error
	Shutdown()
}

type State struct {
	Bot Sender `json:"-"`

	Handlers HandlerMap `json:"-"`

	PrevTick int
	Tick     int
	Joined   bool
	// your car id
	Car CarId
	// race info
	Track   Track
	Cars    map[string]CarInfo
	Session Session
	// debug info
	LastThrottle float
	LastPosition CarPosition
	// AI information
	Speed    float
	MaxSpeed float

	MaxAcceleration float
	MaxDeceleration float

	Markers *Markers
}

func (s *State) Init() {
	s.Cars = make(map[string]CarInfo)
	s.Handlers = HandlerMap{
		// startup
		"join":       s.HandleJoin,
		"createRace": s.HandleCreateRace,
		"error":      s.HandleError,
		// shutdown
		"tournamentEnd": s.HandleTournamentEnd,
		// game startup
		"yourCar":   s.HandleYourCar,
		"gameInit":  s.HandleGameInit,
		"gameStart": s.HandleGameStart,
		// main update
		"carPositions": s.HandleCarPositions,
		// special events
		"crash":          s.HandleCrash,
		"turboAvailable": s.RespondPing,
		// lapping
		"lapFinished": s.HandleLapFinished,
		"finish":      s.RespondPing,
		// game finishing
		"gameEnd": s.RespondPing,
	}

	s.MaxAcceleration = 1.0
	s.MaxDeceleration = -0.002
}

func (s *State) Handle(m *Inbound) error {
	if m.GameTick > 0 {
		s.PrevTick = s.Tick
		s.Tick = m.GameTick
	}

	if m.Type != "carPositions" {
		log.Printf("%s\n", m.Type)
	}

	handler, ok := s.Handlers[m.Type]
	if !ok {
		log.Printf("Error: Unknown message %+v", m)
		return s.ping()
	}

	return handler(m)
}

// Basic handling

func (s *State) HandleError(m *Inbound) error {
	message := string(m.Data)
	log.Printf("Got error: %v\n", message)
	s.Bot.Send(&Ping{})
	return nil
}

func (s *State) RespondPing(m *Inbound) error {
	return s.ping()
}

func (s *State) ping() error {
	return s.Bot.Send(&Ping{})
}

func (s *State) HandleJoin(m *Inbound) error {
	log.Printf("Joined\n")
	s.Joined = true
	return nil
}

func (s *State) HandleCreateRace(m *Inbound) error {
	log.Printf("Created\n")
	s.Bot.Send(&JoinRace{s.Bot.Id(), *track, *password, 1})
	s.Joined = true
	return nil
}

func (s *State) HandleTournamentEnd(m *Inbound) error {
	s.Bot.Shutdown()
	return nil
}

func (s *State) HandleYourCar(m *Inbound) error {
	err := json.Unmarshal(m.Data, &s.Car)
	if err != nil {
		log.Printf("Error: yourCar %v\n", err)
	}
	return nil
}

func (s *State) HandleCrash(m *Inbound) error {
	car := CarId{}
	err := json.Unmarshal(m.Data, &car)
	if err != nil {
		log.Printf("Error: crash %v\n", err)
		return nil
	}
	if car.Name != s.Car.Name {
		return nil
	}

	log.Printf("Crash T:%v, P:%+v\n", s.LastThrottle, s.LastPosition)
	return nil
}

func (s *State) HandleLapFinished(m *Inbound) error {
	finished := LapFinished{}
	err := json.Unmarshal(m.Data, &finished)
	if err != nil {
		log.Printf("Error: lapFinished %v\n", err)
		return nil
	}
	if finished.Car.Name == s.Car.Name {
		log.Printf("\tLAP %vt %v %vms\n", finished.LapTime.Lap, finished.LapTime.Ticks, finished.LapTime.Millis)
	}
	return nil
}
