package main

import (
	"flag"
	"log"
	"net"
	"os"
)

var (
	host = flag.String("host", os.Getenv("BOTHOST"), "hostname for the bot")
	name = flag.String("name", os.Getenv("BOTNAME"), "name for the bot")
	key  = flag.String("key", os.Getenv("BOTKEY"), "key for the bot")

	track    = flag.String("track", os.Getenv("TRACK"), "track name")
	password = flag.String("password", os.Getenv("TRACKPASSWORD"), "track password")

	verbose = flag.Bool("verbose", false, "verbose logging")
)

func parseargs() {
	args := os.Args[1:]
	if len(args) != 4 {
		return
	}

	*host = args[0] + ":" + args[1]
	*name = args[2]
	*key = args[3]
}

func main() {
	flag.Parse()

	if *host == "" || *name == "" || *key == "" {
		parseargs()
	}

	log.SetPrefix(*track + " ")
	log.SetFlags(0)

	if *host == "" || *name == "" || *key == "" {
		flag.PrintDefaults()
		log.Fatal("One of the parameters was missing\nhost=%v, bot name=%v, key=%v", *host, *name, *key)
		os.Exit(1)
	}

	log.Println("Connecting with parameters:")
	log.Printf("host=%v, bot name=%v, key=%v\n", *host, *name, *key)
	conn, err := net.Dial("tcp", *host)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	log.Printf("Connected\n")

	bot := NewBot(conn, *name, *key)
	bot.Run()

	log.Println("Bot finished gracefully.")
}
