package main

import "math"

type float float32

func exp(v float) float {
	return float(math.Exp(float64(v)))
}

func pow(a, b float) float {
	return float(math.Pow(float64(a), float64(b)))
}

func delta(a, b float) float {
	return abs(a - b)
}

func radians(v float) float {
	return v * math.Pi / 180.0
}

func abs(a float) float {
	if a >= 0 {
		return a
	}
	return -a
}

func min(a, b float) float {
	if a <= b {
		return a
	}
	return b
}

func max(a, b float) float {
	if a >= b {
		return a
	}
	return b
}
