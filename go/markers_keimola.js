var keimola = {
    "PrevTick": 0,
    "Tick": 0,
    "Joined": true,
    "Car": {
        "name": "Phi",
        "color": "red"
    },
    "Track": {
        "id": "keimola",
        "name": "Keimola",
        "pieces": [
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": true,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 0,
                "switch": true,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": -22.5
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": true,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": -45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": -45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": -45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": -45
            },
            {
                "length": 100,
                "switch": true,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": -22.5
            },
            {
                "length": 100,
                "switch": true,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 62,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": true,
                "radius": 100,
                "angle": -45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": -45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 100,
                "switch": true,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 90,
                "switch": false,
                "radius": 0,
                "angle": 0
            }
        ],
        "lanes": [
            {
                "index": 0,
                "distanceFromCenter": -10
            },
            {
                "index": 1,
                "distanceFromCenter": 10
            }
        ],
        "startingPoint": {
            "position": {
                "x": -300,
                "y": -44
            },
            "angle": 90
        }
    },
    "Cars": {
        "Phi": {
            "id": {
                "name": "Phi",
                "color": "red"
            },
            "Dimensions": {
                "length": 40,
                "width": 20,
                "guideFlagPosition": 10
            }
        }
    },
    "Session": {
        "laps": 3,
        "maxLapTimeMs": 60000,
        "quickRace": true
    },
    "LastThrottle": 0,
    "LastPosition": {
        "id": {
            "name": "",
            "color": ""
        },
        "angle": 0,
        "piecePosition": {
            "pieceIndex": 0,
            "inPieceDistance": 0,
            "lane": {
                "startLaneIndex": 0,
                "endLaneIndex": 0
            },
            "lap": 0
        }
    },
    "Speed": 0,
    "MaxSpeed": 0,
    "MaxAcceleration": 1,
    "MaxDeceleration": -0.002,
    "Markers": {
        "M": [
            {
                "index": 0,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 0,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 0,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 0,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 1,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 1,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 1,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 1,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 2,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 2,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 2,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 2,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 3,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 3,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 3,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 3,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 4,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 4,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 4,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 5,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 5,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 5,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 6,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 6,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 6,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 7,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 7,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 7,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 8,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 8,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 8,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 9,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 9,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 9,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 9,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 10,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 10,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 10,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 10,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": -0
            },
            {
                "index": 11,
                "startAt": 0,
                "length": 30,
                "angle": -8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.15
            },
            {
                "index": 11,
                "startAt": 30,
                "length": 30,
                "angle": -8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.15
            },
            {
                "index": 11,
                "startAt": 60,
                "length": 18.539818,
                "angle": -5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.092699096
            },
            {
                "index": 12,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 12,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 12,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 12,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 13,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 13,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 13,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 13,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": -0
            },
            {
                "index": 14,
                "startAt": 0,
                "length": 30,
                "angle": -17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.3
            },
            {
                "index": 14,
                "startAt": 30,
                "length": 30,
                "angle": -17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.3
            },
            {
                "index": 14,
                "startAt": 60,
                "length": 18.539818,
                "angle": -10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.18539819
            },
            {
                "index": 15,
                "startAt": 0,
                "length": 30,
                "angle": -17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.3
            },
            {
                "index": 15,
                "startAt": 30,
                "length": 30,
                "angle": -17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.3
            },
            {
                "index": 15,
                "startAt": 60,
                "length": 18.539818,
                "angle": -10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.18539819
            },
            {
                "index": 16,
                "startAt": 0,
                "length": 30,
                "angle": -17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.3
            },
            {
                "index": 16,
                "startAt": 30,
                "length": 30,
                "angle": -17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.3
            },
            {
                "index": 16,
                "startAt": 60,
                "length": 18.539818,
                "angle": -10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.18539819
            },
            {
                "index": 17,
                "startAt": 0,
                "length": 30,
                "angle": -17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.3
            },
            {
                "index": 17,
                "startAt": 30,
                "length": 30,
                "angle": -17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.3
            },
            {
                "index": 17,
                "startAt": 60,
                "length": 18.539818,
                "angle": -10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.18539819
            },
            {
                "index": 18,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 18,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 18,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 18,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 19,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 19,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 19,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 20,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 20,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 20,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 21,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 21,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 21,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 22,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 22,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 22,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 23,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 23,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 23,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.092699096,
                "driftRight": -0
            },
            {
                "index": 24,
                "startAt": 0,
                "length": 30,
                "angle": -8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.15
            },
            {
                "index": 24,
                "startAt": 30,
                "length": 30,
                "angle": -8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.15
            },
            {
                "index": 24,
                "startAt": 60,
                "length": 18.539818,
                "angle": -5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.092699096
            },
            {
                "index": 25,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 25,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 25,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 25,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 26,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 26,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 26,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 27,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 27,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 27,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 28,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 28,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 28,
                "startAt": 60,
                "length": 2,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": -0
            },
            {
                "index": 29,
                "startAt": 0,
                "length": 30,
                "angle": -17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.3
            },
            {
                "index": 29,
                "startAt": 30,
                "length": 30,
                "angle": -17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.3
            },
            {
                "index": 29,
                "startAt": 60,
                "length": 18.539818,
                "angle": -10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.18539819
            },
            {
                "index": 30,
                "startAt": 0,
                "length": 30,
                "angle": -17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.3
            },
            {
                "index": 30,
                "startAt": 30,
                "length": 30,
                "angle": -17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.3
            },
            {
                "index": 30,
                "startAt": 60,
                "length": 18.539818,
                "angle": -10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": -0.18539819
            },
            {
                "index": 31,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 31,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 31,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 32,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 32,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 32,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 33,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 33,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 33,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 34,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 34,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 1.6438627,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 34,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 5,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 35,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 35,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 35,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 35,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 36,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 36,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 36,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 36,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 37,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 37,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 37,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 37,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 38,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 38,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 38,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 38,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 39,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 39,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 39,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 100,
                "driftLeft": 0,
                "driftRight": 0
            }
        ],
        "At": [
            0,
            4,
            8,
            12,
            16,
            19,
            22,
            25,
            28,
            31,
            35,
            39,
            42,
            46,
            50,
            53,
            56,
            59,
            62,
            66,
            69,
            72,
            75,
            78,
            81,
            84,
            88,
            91,
            94,
            97,
            100,
            103,
            106,
            109,
            112,
            115,
            119,
            123,
            127,
            131
        ]
    }
};