var usa = {
    "PrevTick": 0,
    "Tick": 0,
    "Joined": true,
    "Car": {
        "name": "Phi",
        "color": "red"
    },
    "Track": {
        "id": "usa",
        "name": "USA",
        "pieces": [
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": true,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": true,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": true,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": true,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 0,
                "switch": false,
                "radius": 200,
                "angle": 22.5
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            }
        ],
        "lanes": [
            {
                "index": 0,
                "distanceFromCenter": -20
            },
            {
                "index": 1,
                "distanceFromCenter": 0
            },
            {
                "index": 2,
                "distanceFromCenter": 20
            }
        ],
        "startingPoint": {
            "position": {
                "x": -340,
                "y": -96
            },
            "angle": 90
        }
    },
    "Cars": {
        "Phi": {
            "id": {
                "name": "Phi",
                "color": "red"
            },
            "Dimensions": {
                "length": 40,
                "width": 20,
                "guideFlagPosition": 10
            }
        }
    },
    "Session": {
        "laps": 3,
        "maxLapTimeMs": 60000,
        "quickRace": true
    },
    "LastThrottle": 0,
    "LastPosition": {
        "id": {
            "name": "",
            "color": ""
        },
        "angle": 0,
        "piecePosition": {
            "pieceIndex": 0,
            "inPieceDistance": 0,
            "lane": {
                "startLaneIndex": 0,
                "endLaneIndex": 0
            },
            "lap": 0
        }
    },
    "Speed": 0,
    "MaxSpeed": 0,
    "MaxAcceleration": 1,
    "MaxDeceleration": -0.002,
    "Markers": {
        "M": [
            {
                "index": 0,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 0,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 0,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 0,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 1,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 1,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 1,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 1,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 2,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 2,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 2,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 3,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 3,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 3,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 4,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 4,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 4,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 5,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 5,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 5,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 6,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 6,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 6,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 6,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 7,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 7,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 7,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 7,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 8,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 8,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 8,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 9,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 9,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 9,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 10,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 10,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 10,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 11,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 11,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 11,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 12,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 12,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 12,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 12,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 13,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 13,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 13,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 13,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 14,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 14,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 14,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 14,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 15,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 15,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 15,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 15,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 16,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 16,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 16,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 16,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 17,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 17,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 17,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 17,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 18,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 18,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 18,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 19,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 19,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 19,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 20,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 20,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 20,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 21,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 21,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 21,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 22,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 22,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 22,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 22,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 23,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 23,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 23,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 23,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 24,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 24,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 24,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 25,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 25,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 25,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 26,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 26,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 26,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 27,
                "startAt": 0,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.0328484,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 27,
                "startAt": 30,
                "length": 30,
                "angle": 8.594367,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 2.8669345,
                "driftLeft": 0.15,
                "driftRight": 0
            },
            {
                "index": 27,
                "startAt": 60,
                "length": 18.539818,
                "angle": 5.3112664,
                "radius": 200,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 5,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 28,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 28,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 28,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 28,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 29,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 29,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 29,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 29,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 30,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 30,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 30,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 30,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 31,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 31,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 31,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 2.0328484,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 31,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 100,
                "driftLeft": 0,
                "driftRight": 0
            }
        ],
        "At": [
            0,
            4,
            8,
            11,
            14,
            17,
            20,
            24,
            28,
            31,
            34,
            37,
            40,
            44,
            48,
            52,
            56,
            60,
            64,
            67,
            70,
            73,
            76,
            80,
            84,
            87,
            90,
            93,
            96,
            100,
            104,
            108
        ]
    }
};