var germany = {
    "PrevTick": 0,
    "Tick": 0,
    "Joined": true,
    "Car": {
        "name": "Phi",
        "color": "red"
    },
    "Track": {
        "id": "germany",
        "name": "Germany",
        "pieces": [
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": true,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": 45
            },
            {
                "length": 50,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": -45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": -45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": -45
            },
            {
                "length": 50,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": -45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": -45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": -22.5
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": 45
            },
            {
                "length": 100,
                "switch": true,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": -45
            },
            {
                "length": 50,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 22.5
            },
            {
                "length": 100,
                "switch": true,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": 45
            },
            {
                "length": 100,
                "switch": true,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": -45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": -45
            },
            {
                "length": 50,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 22.5
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 50,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": -22.5
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": -22.5
            },
            {
                "length": 100,
                "switch": true,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 22.5
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 70,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": true,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": -45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 50,
                "angle": -45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 50,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 0,
                "switch": false,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 0,
                "switch": true,
                "radius": 100,
                "angle": 45
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 100,
                "switch": false,
                "radius": 0,
                "angle": 0
            },
            {
                "length": 59,
                "switch": false,
                "radius": 0,
                "angle": 0
            }
        ],
        "lanes": [
            {
                "index": 0,
                "distanceFromCenter": -10
            },
            {
                "index": 1,
                "distanceFromCenter": 10
            }
        ],
        "startingPoint": {
            "position": {
                "x": -12,
                "y": 192
            },
            "angle": -90
        }
    },
    "Cars": {
        "Phi": {
            "id": {
                "name": "Phi",
                "color": "red"
            },
            "Dimensions": {
                "length": 40,
                "width": 20,
                "guideFlagPosition": 10
            }
        }
    },
    "Session": {
        "laps": 3,
        "maxLapTimeMs": 60000,
        "quickRace": true
    },
    "LastThrottle": 0,
    "LastPosition": {
        "id": {
            "name": "",
            "color": ""
        },
        "angle": 0,
        "piecePosition": {
            "pieceIndex": 0,
            "inPieceDistance": 0,
            "lane": {
                "startLaneIndex": 0,
                "endLaneIndex": 0
            },
            "lap": 0
        }
    },
    "Speed": 0,
    "MaxSpeed": 0,
    "MaxAcceleration": 1,
    "MaxDeceleration": -0.002,
    "Markers": {
        "M": [
            {
                "index": 0,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 0,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 0,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 0,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 1,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 1,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 1,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 1,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 2,
                "startAt": 0,
                "length": 30,
                "angle": 34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.6,
                "driftRight": 0
            },
            {
                "index": 2,
                "startAt": 30,
                "length": 9.269909,
                "angle": 10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 3,
                "startAt": 0,
                "length": 30,
                "angle": 34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.6,
                "driftRight": 0
            },
            {
                "index": 3,
                "startAt": 30,
                "length": 9.269909,
                "angle": 10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 4,
                "startAt": 0,
                "length": 30,
                "angle": 34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.6,
                "driftRight": 0
            },
            {
                "index": 4,
                "startAt": 30,
                "length": 9.269909,
                "angle": 10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 5,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 5,
                "startAt": 30,
                "length": 20,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": -0
            },
            {
                "index": 6,
                "startAt": 0,
                "length": 30,
                "angle": -34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.6
            },
            {
                "index": 6,
                "startAt": 30,
                "length": 9.269909,
                "angle": -10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.18539819
            },
            {
                "index": 7,
                "startAt": 0,
                "length": 30,
                "angle": -34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.6
            },
            {
                "index": 7,
                "startAt": 30,
                "length": 9.269909,
                "angle": -10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.18539819
            },
            {
                "index": 8,
                "startAt": 0,
                "length": 30,
                "angle": -34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.6
            },
            {
                "index": 8,
                "startAt": 30,
                "length": 9.269909,
                "angle": -10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.18539819
            },
            {
                "index": 9,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 9,
                "startAt": 30,
                "length": 20,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": -0
            },
            {
                "index": 10,
                "startAt": 0,
                "length": 30,
                "angle": -34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.6
            },
            {
                "index": 10,
                "startAt": 30,
                "length": 9.269909,
                "angle": -10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.18539819
            },
            {
                "index": 11,
                "startAt": 0,
                "length": 30,
                "angle": -34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.6
            },
            {
                "index": 11,
                "startAt": 30,
                "length": 9.269909,
                "angle": -10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.18539819
            },
            {
                "index": 12,
                "startAt": 0,
                "length": 30,
                "angle": -17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.3
            },
            {
                "index": 12,
                "startAt": 30,
                "length": 9.269909,
                "angle": -5.3112664,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": -0.092699096
            },
            {
                "index": 13,
                "startAt": 0,
                "length": 30,
                "angle": 34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.6,
                "driftRight": 0
            },
            {
                "index": 13,
                "startAt": 30,
                "length": 9.269909,
                "angle": 10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 14,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 14,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 14,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 14,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 15,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 15,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 15,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 15,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": -0
            },
            {
                "index": 16,
                "startAt": 0,
                "length": 30,
                "angle": -34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.6
            },
            {
                "index": 16,
                "startAt": 30,
                "length": 9.269909,
                "angle": -10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.18539819
            },
            {
                "index": 17,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 17,
                "startAt": 30,
                "length": 20,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 18,
                "startAt": 0,
                "length": 30,
                "angle": 34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.6,
                "driftRight": 0
            },
            {
                "index": 18,
                "startAt": 30,
                "length": 9.269909,
                "angle": 10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 19,
                "startAt": 0,
                "length": 30,
                "angle": 34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.6,
                "driftRight": 0
            },
            {
                "index": 19,
                "startAt": 30,
                "length": 9.269909,
                "angle": 10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 20,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 20,
                "startAt": 30,
                "length": 9.269909,
                "angle": 5.3112664,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 21,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 21,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 21,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 21,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 22,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 22,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 22,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 22,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 23,
                "startAt": 0,
                "length": 30,
                "angle": 34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.6,
                "driftRight": 0
            },
            {
                "index": 23,
                "startAt": 30,
                "length": 9.269909,
                "angle": 10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 24,
                "startAt": 0,
                "length": 30,
                "angle": 34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.6,
                "driftRight": 0
            },
            {
                "index": 24,
                "startAt": 30,
                "length": 9.269909,
                "angle": 10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 25,
                "startAt": 0,
                "length": 30,
                "angle": 34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.6,
                "driftRight": 0
            },
            {
                "index": 25,
                "startAt": 30,
                "length": 9.269909,
                "angle": 10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 26,
                "startAt": 0,
                "length": 30,
                "angle": 34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.6,
                "driftRight": 0
            },
            {
                "index": 26,
                "startAt": 30,
                "length": 9.269909,
                "angle": 10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 27,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 27,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 27,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 27,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": -0
            },
            {
                "index": 28,
                "startAt": 0,
                "length": 30,
                "angle": -34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.6
            },
            {
                "index": 28,
                "startAt": 30,
                "length": 9.269909,
                "angle": -10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.18539819
            },
            {
                "index": 29,
                "startAt": 0,
                "length": 30,
                "angle": -34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.6
            },
            {
                "index": 29,
                "startAt": 30,
                "length": 9.269909,
                "angle": -10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.18539819
            },
            {
                "index": 30,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 30,
                "startAt": 30,
                "length": 20,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 31,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 31,
                "startAt": 30,
                "length": 9.269909,
                "angle": 5.3112664,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 32,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 32,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 32,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 32,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 33,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 33,
                "startAt": 30,
                "length": 20,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": -0
            },
            {
                "index": 34,
                "startAt": 0,
                "length": 30,
                "angle": -17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.3
            },
            {
                "index": 34,
                "startAt": 30,
                "length": 9.269909,
                "angle": -5.3112664,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.092699096
            },
            {
                "index": 35,
                "startAt": 0,
                "length": 30,
                "angle": -17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.3
            },
            {
                "index": 35,
                "startAt": 30,
                "length": 9.269909,
                "angle": -5.3112664,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.092699096
            },
            {
                "index": 36,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 36,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 36,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 36,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 37,
                "startAt": 0,
                "length": 30,
                "angle": 34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.6,
                "driftRight": 0
            },
            {
                "index": 37,
                "startAt": 30,
                "length": 9.269909,
                "angle": 10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 38,
                "startAt": 0,
                "length": 30,
                "angle": 34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.6,
                "driftRight": 0
            },
            {
                "index": 38,
                "startAt": 30,
                "length": 9.269909,
                "angle": 10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 39,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 39,
                "startAt": 30,
                "length": 9.269909,
                "angle": 5.3112664,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.092699096,
                "driftRight": 0
            },
            {
                "index": 40,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 40,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 40,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 40,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 41,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 41,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 41,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 41,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 42,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 42,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 42,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 42,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 43,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 43,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 43,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 44,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 44,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 44,
                "startAt": 60,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 45,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 45,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 45,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 45,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": -0
            },
            {
                "index": 46,
                "startAt": 0,
                "length": 30,
                "angle": -34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.6
            },
            {
                "index": 46,
                "startAt": 30,
                "length": 9.269909,
                "angle": -10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.1366186,
                "driftLeft": -0,
                "driftRight": -0.18539819
            },
            {
                "index": 47,
                "startAt": 0,
                "length": 30,
                "angle": -34.37747,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": -0,
                "driftRight": -0.6
            },
            {
                "index": 47,
                "startAt": 30,
                "length": 9.269909,
                "angle": -10.622533,
                "radius": 50,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": -0.18539819
            },
            {
                "index": 48,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 48,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 48,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 49,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 49,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 49,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 50,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 50,
                "startAt": 30,
                "length": 20,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.8264944,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 51,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 51,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 51,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 52,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 52,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 52,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 53,
                "startAt": 0,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 0.8264944,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 53,
                "startAt": 30,
                "length": 30,
                "angle": 17.188734,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 1.6438627,
                "driftLeft": 0.3,
                "driftRight": 0
            },
            {
                "index": 53,
                "startAt": 60,
                "length": 18.539818,
                "angle": 10.622533,
                "radius": 100,
                "maxEntrySpeed": 0.5,
                "targetSpeed": 5,
                "driftLeft": 0.18539819,
                "driftRight": 0
            },
            {
                "index": 54,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 54,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 54,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 54,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 55,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 55,
                "startAt": 30,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 55,
                "startAt": 60,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 55,
                "startAt": 90,
                "length": 10,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 5,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 56,
                "startAt": 0,
                "length": 30,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 0.1366186,
                "driftLeft": 0,
                "driftRight": 0
            },
            {
                "index": 56,
                "startAt": 30,
                "length": 29,
                "angle": 0,
                "radius": 0,
                "maxEntrySpeed": 100,
                "targetSpeed": 100,
                "driftLeft": 0,
                "driftRight": 0
            }
        ],
        "At": [
            0,
            4,
            8,
            10,
            12,
            14,
            16,
            18,
            20,
            22,
            24,
            26,
            28,
            30,
            32,
            36,
            40,
            42,
            44,
            46,
            48,
            50,
            54,
            58,
            60,
            62,
            64,
            66,
            70,
            72,
            74,
            76,
            78,
            82,
            84,
            86,
            88,
            92,
            94,
            96,
            98,
            102,
            106,
            110,
            113,
            116,
            120,
            122,
            124,
            127,
            130,
            132,
            135,
            138,
            141,
            145,
            149
        ]
    }
};