'use strict';

var Screen = {
	size: {
		x: window.innerWidth - 30,
		y: window.innerHeight - 30
	}
};

var canvas = document.getElementById("view");
canvas.width = Screen.size.x;
canvas.height = Screen.size.y;

var context = canvas.getContext("2d");

function foreach(obj, fn) {
	for (var id in obj) {
		if (!obj.hasOwnProperty(id)) continue;
		fn(id, obj[id]);
	}
}

var tracks = [
  keimola,
  france,
  germany,
  usa
];


var TAU = 2 * Math.PI;

function radians(v){
  return v * TAU / 360;
}
var totalWidth = 10 * 6;

var max = Math.max;
var min = Math.min;
var cos = Math.cos;
var sin = Math.sin;

var trackLayer = {
  all: function(context, track, piece){
    //context.strokeStyle = "black";
    var speed = min(piece.targetSpeed, 3.0);
    var rot = (speed*90) | 0;
    context.strokeStyle = "hsla(" + rot + ", 70%, 50%, 1.0)";
    // console.log("hsla(" + (piece.targetSpeed * 30)|0 + ",50%, 50%, 1)");
	  context.lineWidth = totalWidth;
  },
  straight: function(context, track, piece){
    context.beginPath();
		context.moveTo(0, 0);
		context.lineTo(0, piece.length);
		context.stroke();
  },
  bend: function(context, track, piece){
    var rads = radians(piece.angle);
		context.beginPath();
		if (rads > 0) {
			context.arc(-piece.radius, 0, piece.radius, 0, rads, 0);
		} else {
			context.arc(piece.radius, 0, piece.radius, TAU / 2, TAU / 2 + rads, 1);
		}
		context.stroke();
  }
};

var laneLayer = {
  all: function(context, track, piece){
    context.strokeStyle = "white";
	  context.lineWidth = 3.0;
  },
  straight: function(context, track, piece){
    context.beginPath();
    for(var i = 0; i < track.lanes.length; i += 1){
      var lane = track.lanes[i];
      context.moveTo(lane.distanceFromCenter, 0);
      context.lineTo(lane.distanceFromCenter, piece.length);
    }
    context.stroke();

    if(piece.switch){
      context.beginPath();
      for(var i = 0; i < track.lanes.length - 1; i += 1){
        var lane = track.lanes[i],
            next = track.lanes[i+1];
        context.moveTo(lane.distanceFromCenter, 0);
        context.lineTo(next.distanceFromCenter, piece.length);
        context.moveTo(next.distanceFromCenter, 0);
        context.lineTo(lane.distanceFromCenter, piece.length);
      }
      context.stroke();
    }
  },
  bend: function(context, track, piece){
    var rads = radians(piece.angle);

    for(var i = 0; i < track.lanes.length; i += 1){
      var lane = track.lanes[i];
      var radius = piece.radius - lane.distanceFromCenter;
      context.beginPath();
      if (rads > 0) {
        context.arc(-piece.radius, 0, radius, 0, rads, 0);
      } else {
        context.arc(piece.radius, 0, radius, TAU / 2, TAU / 2 + rads, 1);
      }
      context.stroke();
    }
  }
};

var pieceIndexLayer = {
  all: function(context, track, piece, i){
    context.font = "20px bold monospace";
  },
  straight: function(context, track, piece, i){
    context.fillStyle = "black";
    context.fillText(piece.targetSpeed.toFixed(2), totalWidth/2+30, 15);
    context.fillStyle = "black";
    context.fillRect(-totalWidth/2-5, 0, totalWidth+20, 2);
  },
  bend: function(context, track, piece, i){
    //context.fillStyle = "black";
    //context.fillText(piece.targetSpeed.toFixed(2), totalWidth/2+30, 15);
    context.fillStyle = "black";
    context.fillRect(-totalWidth/2-5, 0, totalWidth+20, 2);
  }
};

var angleLayer = {
  all: function(context, track, piece){
    context.strokeStyle = "#000";
    context.lineWidth = 3;
    context.beginPath();

    var R = 50;
    context.moveTo(
      R*cos(TAU/4 - piece.driftLeft),
      -R*sin(TAU/4 - piece.driftLeft));

    context.lineTo(0, 0);

    context.lineTo(
      R*cos(TAU/4 - piece.driftRight),
      -R*sin(TAU/4 - piece.driftRight));

    context.stroke();
  }
};

var transformLayer = {
  straight: function(context, track, piece){
    context.translate(0, piece.length);
  },
  bend: function(context, track, piece){
    var rads = radians(piece.angle);
    if (rads > 0) {
			context.translate(-piece.radius * (1 - Math.cos(rads)), piece.radius * Math.sin(rads));
		} else {
			context.translate(piece.radius * (1 - Math.cos(-rads)), piece.radius * Math.sin(-rads));
		}
		context.rotate(rads);
  }
};

var layers = [
  trackLayer,
  laneLayer,
  //pieceIndexLayer,
  angleLayer
];

function reset(markers){
  var N = markers.length;
  for(var i = 0; i < markers.length; i += 1){
    markers[i].maxEntrySpeed = markers[i].radius > 0 ? 0.5 : 3.0;
    markers[i].targetSpeed = markers[i].maxEntrySpeed;
    markers[i].driftLeft = 0;
    markers[i].driftRight = 0;
  }
}

function delta(a,b){
  if( a > b ){
    return a - b;
  } else {
    return b - a;
  }
}

function updateDrifts(markers){
  var N = markers.length;

  var falloff = 1/2,
      distance = 70;

  for(var i = N - 1; i >= 0; i -= 1){
    var next = markers[i];
    var cur = markers[(i - 1 + N)%N];

    var m = Math.pow(falloff, cur.length / distance);

    cur.driftLeft = (radians(next.angle) + next.driftLeft) * m;
    cur.driftRight = (radians(next.angle) + next.driftRight) * m;

    switch(2){
      case 1:
        cur.driftLeft = max(cur.driftLeft, 0);
        cur.driftRight = min(cur.driftRight, 0);
        break;
      case 2:
        cur.driftLeft = max(cur.driftLeft, radians(next.angle));
        cur.driftRight = min(cur.driftRight, -radians(next.angle));
        break;
      case 3:
        cur.driftLeft = max(cur.driftLeft, radians(next.angle)*m);
        cur.driftRight = min(cur.driftRight, -radians(next.angle)*m);
        break;
    }

    cur.driftLeft = cur.driftLeft * 0.9;
    cur.driftRight = cur.driftRight * 0.9;
  }
}

function updateTargetSpeed(markers){
  var N = markers.length;

  var mul = 5.0;

  for(var i = N - 1; i >= 0; i -= 1){
    var next = markers[i];
    var cur = markers[(i - 1 + N)%N];

    var df = delta(next.driftLeft, next.driftRight);
    cur.targetSpeed = Math.exp(-df*2);
  }
}

function smoothenTargetSpeed(markers){
  var N = markers.length;

  for(var i = N - 1; i >= 0; i -= 1){
    var next = markers[i];
    var cur = markers[(i - 1 + N)%N];
    var r = Math.pow(1/2, cur.length/100);
    cur.targetSpeed = min(cur.targetSpeed, cur.targetSpeed*r + next.targetSpeed*(1-r));
  }
}

function renderState(context) {
  context.fillStyle = "#222";
  context.fillRect(0,0,5000, 5000);
  context.clearRect(0,0,5000, 5000)

  for(var k = 0; k < tracks.length; k += 1){
    var STATE = tracks[k];
    reset(STATE.Markers.M);

    updateDrifts(STATE.Markers.M);
    updateDrifts(STATE.Markers.M);
    updateTargetSpeed(STATE.Markers.M);
    smoothenTargetSpeed(STATE.Markers.M);
    smoothenTargetSpeed(STATE.Markers.M);

    context.save();

    var row = k % 2;
    var col = (k / 2)|0;

    context.translate(400 + 800*row, 200 + 400 * col);
    var s = 0.5;
    context.scale(s,s);
    context.rotate(-TAU / 4);

    var pieces = STATE.Markers.M;
    var track = STATE.Track;

    var tr = transformLayer;

    for(var j = 0; j < layers.length; j += 1){
      var layer = layers[j];
      context.save();
      for(var i = 0; i < pieces.length; i += 1){
        var piece = pieces[i];
        layer.all && layer.all(context, track, piece, i);

        var kind = piece.radius > 0 ? "bend" : "straight";
        layer[kind] && layer[kind](context, track, piece, i);
        tr[kind] && tr[kind](context, track, piece, i);
      }
      context.restore();
    }

    context.restore();
  }
}

function render(context) {
	context.clearRect(0, 0, 100000, 100000);
	renderState(context);
};

setInterval(function(){render(context)}, 500);
